package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;



public class DotsAndBoxesGridTest {

    // FIXME: You need to write tests for the two known bugs in the code.
    private DotsAndBoxesGrid grid;

    @BeforeEach
    public void setUp() {
        // Create a new DotsAndBoxesGrid instance before each test
    	grid = new DotsAndBoxesGrid(15, 8, 2);
    }

    @Test
    public void testBoxComplete() {
        // Test if the boxComplete method works correctly
        grid.drawHorizontal(3,3,1);
        grid.drawHorizontal(3,4,1);
        grid.drawVertical(3,3,1);
        grid.drawVertical(4,3,1);
        assertTrue(grid.boxComplete(3,3));
    }

    @Test
   public void whenExceptionThrown_thenAssertionSucceeds() {
    	grid.drawHorizontal(3, 3, 1);
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            grid.drawHorizontal(3,3,1);
        });

        String expectedMessage = "Already Drawn";
        String actualMessage = exception.getMessage();
        System.out.println(actualMessage);
        assertTrue(actualMessage.contains(expectedMessage));
    }


}
